#!/bin/bash
sudo yum update -y
sudo yum upgrade -y
sudo yum install epel-release -y

sudo timedatectl set-timezone America/Panama

echo "You want Set Host Name yes or no"
read hn

if [ "$hn" == "yes" ]; then
	echo "Type your  Host Name: "
	read hnserver
sudo hostnamectl set-hostname $hnserver
else
	echo "Continue with the setting!"
fi

sudo setenforce 0
sudo sed -i 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/selinux/config

sudo systemctl disable firewalld
sudo systemctl stop firewalld
